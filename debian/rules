#! /usr/bin/make -f

# debian/rules
# Part of Debian ‘python-tinyrpc’ package.
#
# This is free software, and you are welcome to redistribute it under
# certain conditions; see the end of this file for copyright
# information, grant of license, and disclaimer of warranty.

# Send HTTP traffic to the “discard” service during packaging actions.
export http_proxy = http://127.0.1.1:9/
export https_proxy = ${http_proxy}

export PYBUILD_NAME = tinyrpc

DOCUMENTATION_DIR = docs
DOCUMENTATION_BUILD_DIR = ${DOCUMENTATION_DIR}/_build
DOCUMENTATION_BUILD_HTML_DIR = ${DOCUMENTATION_BUILD_DIR}/html
GENERATED_FILES += ${DOCUMENTATION_BUILD_DIR}

SPHINX = sphinx-build
SPHINX_OPTS = -N


%:
	dh $@ --with python3,sphinxdoc --buildsystem=pybuild


override_dh_auto_clean:
	dh_auto_clean
	$(RM) -r ${GENERATED_FILES}

override_dh_auto_build:
	dh_auto_build
	$(SPHINX) ${SPHINX_OPTS} -bhtml ${DOCUMENTATION_DIR}/ \
		${DOCUMENTATION_BUILD_HTML_DIR}/


# Copyright © 2009–2022 Ben Finney <bignose@debian.org>
#
# This is free software: you may copy, modify, and/or distribute this work
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; version 3 of that license or any later version.
# No warranty expressed or implied.


# Local variables:
# mode: makefile
# coding: utf-8
# End:
# vim: filetype=make fileencoding=utf-8 :
