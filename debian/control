Source: python-tinyrpc
Maintainer: Ben Finney <bignose@debian.org>
Section: python
Priority: optional
Build-Depends:
    python3-sphinx,
    python3-pytest-runner,
    python3-mock,
    python3-gevent,
    python3-requests,
    python3-websocket,
    python3-jsonext,
    python3-zmq,
    python3-six,
    python3-setuptools,
    python3-all,
    dh-python,
    debhelper-compat (= 13)
Standards-Version: 4.6.0
Homepage: https://github.com/mbr/tinyrpc/
VCS-Git: https://salsa.debian.org/bignose/pkg-python-tinyrpc.git
VCS-Browser: https://salsa.debian.org/bignose/pkg-python-tinyrpc/
Rules-Requires-Root: no

Package: python3-tinyrpc
Architecture: all
Depends:
    ${python3:Depends},
    ${misc:Depends}
Suggests:
    python-tinyrpc-doc
Description: small, modular RPC library — Python 3
 ‘tinyrpc’ is a library for making and handling RPC calls in Python.
 .
 Its initial scope is handling JSON-RPC, although it aims to be very
 well-documented and modular to make it easy to add support for
 further protocols.
 .
 A feature is support of multiple transports (or none at all) and
 providing clever syntactic sugar for writing dispatchers.
 .
 This package installs the library for Python 3.

Package: python-tinyrpc-doc
Architecture: all
Section: doc
Depends:
    ${sphinxdoc:Depends},
    ${misc:Depends}
Description: small, modular RPC library — documentation
 ‘tinyrpc’ is a library for making and handling RPC calls in Python.
 .
 Its initial scope is handling JSON-RPC, although it aims to be very
 well-documented and modular to make it easy to add support for
 further protocols.
 .
 This package installs the library documentation.
